#!/usr/bin/python3

#-----------------------------------------------------------------------------
#   Description: ECC installer
#   Program:     installer.py
#-----------------------------------------------------------------------------
#   This script is not yet a finished product, due to the fact that we have
#   yet to resolve:
#       a. How ECC certs will be created and delivered
#       b. ECC naming convention
#
#   TODO LIST - John Kane - 20190524
#
#       1. Currently, 'ecc50' is hardcoded as the device, certs, etc.  This
#           needs to be taken from /tmp/ecc_mgr_agent.json OR as 
#           the DP+licensekey
#
#       2. ECC cert/key need to be somehow made available to this script.
#           Currently, ecc50's cert/key pair are hardcoded as the certs.
#
#       3. Add the ema process (ecc-mgr-agent) to monit.  The is already a
#           function available (monit_ema) to do that, just need the text
#           for the monit file 'ema'.
#
#-----------------------------------------------------------------------------
# Function:
#   1. Pulls down EMA
#   2. Runs EMA to gather user's creds
#   3. apt installs various necessary SW (ssh, monit, etc)
#   4. Adds users 'ecc' and 'upd'
#   5. Installs docker and docker-compose
#   6. Installs ecc update client
#   7. Installs ecc tunnel client
#
#   Once complete, the ecc should be connected to the IoT hub.
#
#-----------------------------------------------------------------------------
#
# Pre-Reqs:
#
#   sudo apt update -y
#   sudo apt upgrade -y
#   sudo apt dist-upgrade -y
#   sudo reboot -f
#
#   Note: for AWS ec2, you'll need to do something like this
#       sudo DEBIAN_FRONTEND=noninteractive apt-get -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" dist-upgrade
#
#   WE SHOULD PROBABLY make 'apt install monit' a PRE-REQ if it we continue 
#       to have issues with it on various installs
#
#-----------------------------------------------------------------------------


import sys
import apt
from subprocess import call, Popen, PIPE, DEVNULL, STDOUT, run
import pwd
import grp
import json
import getpass
from time import sleep


deviceId=""
tunnelPort=0

# ------------------------------------------------------------
# Check OS
# ------------------------------------------------------------
def os_check():
    # print("Running os_check -------")

    cmd = "lsb_release -a | grep Release | grep 18.04 2>&1 > /dev/null"
    #cmd = "grep VERSION_ID /etc/os-release | grep 18.04 2>&1 > /dev/null"
    p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
    sout = p.stdout.readlines()
    ppoll = p.poll()
    if p.returncode != 0:
        sys.exit("Ubuntu Release 18.04 is REQUIRED")
 
    # print("Finished os_check -------")


# ------------------------------------------------------------
# EMA
# ------------------------------------------------------------
def get_creds():
    # print("Running get_creds -------")

    email = input("email: ")
    pswd = input("password: ")
    lkey = input("license key: ")

    # print("Finished get_creds -------")

    return(email,pswd,lkey)


def ema_install():
    # print("Running ema_install -------")
    apt_install("npm")
    apt_install("nodejs")  # IS THIS NEEDED? - jtk

    # Check if installed
    # NOT SURE WHICH IS BETTER???? I have issue with both
    cmdArgs = ['npm','view','ecc-mgr-agent','version']
    # cmdArgs = ['npm','list','ecc-mgr-agent']
    print("run(cmdArgs,stdout=DEVNULL,stderr=STDOUT).returncode = ");
    print(run(cmdArgs,stdout=DEVNULL,stderr=STDOUT).returncode);
    
    #if run(cmdArgs,stdout=DEVNULL,stderr=STDOUT).returncode != 0:
    cmdArgs = ['npm','install','-g','ecc-mgr-agent']
    if run(cmdArgs,stdout=DEVNULL,stderr=STDOUT).returncode != 0:
        sys.exit("Unable to install ecc-mgr-agent")
        # else:
        #     print("ecc-mgr-agent install successful")
    # else:
    #      print("ecc-mgr-agent already installed")

    # print("Finished ema_install -------")


def ema_activate(email,pswd,lkey):
    print("Running ema_activate -------")

    # Build the command list
    cmdArgs = ['ecc-mgr-agent','activate','','','']
    cmdArgs[2] = "--username=" + email
    cmdArgs[3] = "--password=" + pswd
    cmdArgs[4] = "--deviceid=" + lkey

    print("Activating with credentials:\nemail:\t\t{}\npassword:\t{}\nlkey key:\t{}".format(email,pswd,lkey))
    # run(cmdArgs,stdout=DEVNULL)

    if run(cmdArgs,stderr=DEVNULL).returncode != 0:
        sys.exit("ecc-mgr-agent failed")

    global tunnelPort
    
    deviceId = lkey
    

    # read the tunnel port 
    with open("/tmp/ecc_mgr_agent.json", "r") as content:
        json_data = json.load(content)
        tunnelPort = json_data['tunnel_port']
        

    print("after activation, deviceId : {}, tunnelport : {}".format(deviceId, tunnelPort));
    

    # stores results in /tmp/ecc_mgr_agent.json, like:
    # {"azure-iot-hub":{"token":"SharedAccessSignature sr=echub1.azure-devices.net%2Fdevices%2FHPN-406z03pjvl5m9np&sig=FuJ2C2L9NhuAd0RBuIJEQwxHH7bysXVVa22SBZf%2FeJ0%3D&se=1558283584&skn=saToken","iotHub":"echub1"}}

    print("Finished ema_activate -------")

# TODO: ADD THE EMA to monit - jtk


# ------------------------------------------------------------
# Apt
# ------------------------------------------------------------
def apt_install(package):
    # print("Running apt_install -------")
    # print("    with args:",package)
    
    # cannot stop the output to STDOUT with this module!

    cache = apt.cache.Cache()
    cache.update()
    cache.open()
    pkg = cache[package]
    if not pkg.is_installed:
        pkg.mark_install()
        try:
            cache.commit()
        except Exception as arg:
            # TODO: use of >> is invalid with print function - jtk
            print >> sys.stderr, "Package installation failed [{}]".format(str(arg))


# ------------------------------------------------------------
# PIP
# ------------------------------------------------------------
def pip_install(package,user=''):
    # print("Running pip_install -------")
    # print("    with args:",package,user)

    cmdArgs = ['dpkg','-s','python3-pip']
    if run(cmdArgs,stdout=DEVNULL,stderr=STDOUT).returncode != 0:
        apt_install("python3-pip")

    import pip

    if user == '':
        checkPkg = call("pip3 list --format=columns | grep '^"
                        + package + " '",
                        shell=True,stdout=DEVNULL,stderr=STDOUT)
    else:
        checkPkg = call("sudo runuser -l upd -c \"pip3 list --format=columns | grep '^"
                        + package + " '\"",
                        shell=True,stdout=DEVNULL,stderr=STDOUT)

    if checkPkg != 0:
        if user == '':
            pip.main(['install', package])
        else:
            run(cmdArgs)
            cmdArgs = ['sudo','/usr/bin/pip3','install','docker-compose']
            run(cmdArgs)

    # print("Finished pip_install -------")


# ------------------------------------------------------------
# User add password
# ------------------------------------------------------------
def adduser_pswd(username,password):
    # print("Running adduser_pswd -------")
    # print("    with args:",username,password)

    # Add user if user does not already exist
    try:
        pwd.getpwnam(username)
    except KeyError as e:
        # TODO: local variable 'e' is assigned to but never used - jtk
        cmd = "sudo useradd -m -s /bin/bash -p $(openssl passwd -1 " + password + ") " + username
        p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
        sout = p.stdout.readlines()
        ppoll = p.poll()
        if p.returncode != 0:
            # print("returncode =",p.returncode)
            sys.exit("User add failed")

    # print("Finished adduser_pswd -------")


# ------------------------------------------------------------
# User add, no password
# ------------------------------------------------------------
def adduser_nopswd(username):
    # print("Running adduser_nopswd -------")
    # print("    with args:",username)

    # Add user if user does not already exist
    try:
        pwd.getpwnam(username)
    except KeyError as e:
        # TODO: local variable 'e' is assigned to but never used - jtk
        # UNTESTED, REPLACED THE 'TODO' BELOW
        cmdArgs = ['sudo','useradd','-m','-s','/bin/bash',username]
        if run(cmdArgs).returncode != 0:
            sys.exit("User add failed for user",username)
        
        # TODO: redo with 'run'? - jtk
        # cmd = "sudo useradd -m -s /bin/bash " + username
        # p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
        # sout = p.stdout.readlines()
        # ppoll = p.poll()
        # if p.returncode != 0:
        #     print("returncode =",p.returncode)
        #     sys.exit("User add failed")

    # print("Finished adduser_nopswd -------")


# ------------------------------------------------------------
# Shell commands via Popen
# ------------------------------------------------------------
def shellCmd(cmd):
    # print("Running shellCmd -------")
    # print("    with args:",cmd)

    # This is needed when you have a '|' (pipe) in your arg
    p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
    sout = p.stdout.readlines()
    ppoll = p.poll()
    if p.returncode != 0:
        print("returncode =",p.returncode)
        sys.exit("Command '" + cmd + "' failed")

    # print("Finished shellCmd -------")


# ------------------------------------------------------------
# Install and configure bash-completion
# IS THIS BASH COMPLETION SHIT REALLY NECESSARY????????
# ------------------------------------------------------------
def bash_comp():
    # print("Running bash_comp -------")

    apt_install("bash-completion")

    # TODO: redo with 'run'? - jtk
    # cmd = "cp /etc/bash.bashrc /etc/bash.bashrc.bak"
    # p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
    # sout = p.stdout.readlines()
    # ppoll = p.poll()
    # if p.returncode == 0:
    cmdArgs = ['sudo','cp','/etc/bash.bashrc','/etc/bash.bashrc.bak']
    if run(cmdArgs).returncode == 0:
        tmpf = open("/etc/bash.bashrc").read()
        tmpf = tmpf.replace("#if ! shopt -oq posix; then",
                            "if ! shopt -oq posix; then")
        tmpf = tmpf.replace("#  if [ -f /usr/share/bash-completion/bash_completion ]; then",
                            "  if [ -f /usr/share/bash-completion/bash_completion ]; then")
        tmpf = tmpf.replace("#    . /usr/share/bash-completion/bash_completion",
                            "    . /usr/share/bash-completion/bash_completion")
        tmpf = tmpf.replace("#  elif [ -f /etc/bash_completion ]; then",
                            "  elif [ -f /etc/bash_completion ]; then")
        tmpf = tmpf.replace("#    . /etc/bash_completion",
                            "    . /etc/bash_completion")
        tmpf = tmpf.replace("#  fi", "  fi")
        tmpf = tmpf.replace("#fi", "fi")
        file = open("/etc/bash.bashrc", 'w')
        file.write(tmpf)
        file.close()
    else:
        sys.exit("Backup of /etc/bash.bashrc failed")

    # print("Finished bash_comp -------")


# ------------------------------------------------------------
# Configure host & hostname
# ------------------------------------------------------------
def host_name(name='ecc'):
    # print("Running host_name -------")
    # print("    with args:",name)

    # rename your server
    # Add hostname to hosts file
    cmdArgs = ['cp','/etc/hosts','/etc/hosts.bak']
    run(cmdArgs)
    cmdArgs = ['sed','-i','/127.0.0.1 localhost/a 127.0.1.1 ecc','/etc/hosts']
    run(cmdArgs)

    cmdArgs = ['cp','/etc/hostname','/etc/hostname.bak']
    run(cmdArgs)

    # NOTE: 'echo' cmds cannot be a separate element in list
    # and they NEED the shell
    cmdArg = ['echo ecc > /tmp/hostname.tmp']
    if run(cmdArg, shell=True).returncode != 0:
        sys.exit("Failed to create /tmp/hostname.tmp")

    cmdArgs = ['sudo','mv','/tmp/hostname.tmp','/etc/hostname']
    run(cmdArgs)

    # print("Finished host_name -------")


# ------------------------------------------------------------
# Install and configure Docker Engine
# Sec 4.2/4.6 - Install Docker Daemon,
# ------------------------------------------------------------
def docker_install():
    # print("Running docker_install -------")

    #FD apt_install("docker.io")

    cmdArgs = ["sudo","systemctl","enable","docker"]
    if run(cmdArgs,stdout=DEVNULL).returncode != 0:
        sys.exit("Docker enable failed")

    cmdArgs = ["sudo","usermod","-aG","docker,sudo","ecc"]
    if run(cmdArgs,stdout=DEVNULL).returncode != 0:
        sys.exit("Add ecc user to docker group failed")

    

    # print("Finished docker_install -------")


# ------------------------------------------------------------
# Install docker-compose
# ------------------------------------------------------------
def docker_comp_install():
    # print("Running docker_comp_install -------")

    # 4.4 jumps to 4.7
    # requires the 'upd' user added but run1.sh above

    #FD pip_install("docker-compose","upd")

    # add user to docker group
    cmdArgs = ['sudo','usermod','-aG','docker','upd']
    run(cmdArgs)

    # Check
    cmdArgs = ['sudo','runuser','-l','upd','-c','docker ps']
    if run(cmdArgs,stdout=DEVNULL).returncode != 0:
        sys.exit("'docker ps' cmd failed for user 'upd'")

    cmdArgs = ['runuser','-l','upd','-c','docker-compose version']
    if run(cmdArgs,stdout=DEVNULL).returncode != 0:
        sys.exit("'docker-compose version' cmd failed")

    # print("Finished docker_comp_install -------")


# ------------------------------------------------------------
# Configure time zone
# ------------------------------------------------------------
def set_tzone(tz):
    # print("Running set_tzone -------")
    # print("    with args:",tz)

    cmdArgs = ['sudo','ln','-fs','/usr/share/zoneinfo/',tz,'/etc/localtime']
    run(cmdArgs)
    cmdArgs = ['sudo','dpkg-reconfigure','-f','noninteractive','tzdata']
    run(cmdArgs)
    
    # TODO: replace with 'run' - jtk
    # cmd = "sudo ln -fs /usr/share/zoneinfo/" + tz + " /etc/localtime"
    # shellCmd(cmd)
    # cmd = "sudo dpkg-reconfigure -f noninteractive tzdata"
    # shellCmd(cmd)

    # print("Finished set_tzone -------")


# ------------------------------------------------------------
# SSH modifications
# ------------------------------------------------------------
def ssh_mods():
    # print("Running ssh_mods -------")

    call("sudo ssh-keygen -q -f /etc/ssh/ssh_host_rsa_key.new -N '' -b 4096 -t rsa",shell=True)
    cmdArgs = ['sudo','mv','/etc/ssh/ssh_host_rsa_key.new',
               '/etc/ssh/ssh_host_rsa_key']
    run(cmdArgs)
    cmdArgs = ['sudo','mv','/etc/ssh/ssh_host_rsa_key.new.pub',
               '/etc/ssh/ssh_host_rsa_key.pub']
    run(cmdArgs)
    cmdArgs = ['sudo','cp','/etc/ssh/sshd_config','/etc/ssh/sshd_config.bak']
    run(cmdArgs)
    # TODO: Uncomment these - jtk
    # cmdArgs = ['sed','-i',
    #         's/#PermitRootLogin .*$/PermitRootLogin no/',
    #         '/etc/ssh/sshd_config']
    # run(cmdArgs)
    # TODO: ServerKeyBits is NO LONGER SUPPORTED - jtk
    # cmdArgs = ['sed','-i',
    #         '/PubkeyAuthentication/a # Added by ECC Installer\nServerKeyBits 4096',
    #         '/etc/ssh/sshd_config']
    # run(cmdArgs)

    cmdArgs = ['sudo','service','ssh','restart']
    run(cmdArgs)

    # print("Finished ssh_mods -------")


# ------------------------------------------------------------
# Apt update, upgrade, and dist-upgrade
# ------------------------------------------------------------
def apt_upgrade():
    # print("Running apt_upgrade -------")

    cache = apt.cache.Cache()
    cache.update()
    cache.open(None)
    cache.upgrade()
    cache.upgrade(True)
    cache.commit()

    # print("Finished apt_upgrade -------")


# ------------------------------------------------------------
# run1.sh
# ------------------------------------------------------------
def run1():
    # print("Running run1 -------")

    cmdArgs = ['sudo','grep','-q','^upd','/etc/sudoers']
    if run(cmdArgs).returncode != 0:
        # print("Adding upd to sudoers -------")
        shellCmd("echo 'upd ALL=(ALL) NOPASSWD: ALL' | sudo EDITOR='tee -a' visudo")
    # else:
    #     print("upd already in sudoers")

    cmdArgs = ['sudo','runuser','-l','ecc','-c',
               'tar -xzf ~/ros1/tunnel.tgz -C ~/']
    run(cmdArgs)

    try:
        grp.getgrnam('appuser')
    except KeyError as e:
        # TODO: local variable 'e' is assigned to but never used - jtk
        cmdArgs = ['groupadd','-g','9990','appuser']
        run(cmdArgs)

    adduser_nopswd('upd')

    cmdArgs = ['sudo','usermod','-L','-a','-G','sudo,docker,appuser','upd']
    run(cmdArgs)

    cmdArgs = ['sudo','cp','/home/ecc/ros1/upd.tgz','/tmp/']
    run(cmdArgs)
    cmdArgs = ['sudo','chown','upd:upd','/tmp/upd.tgz']
    run(cmdArgs)

    cmdArgs = ['sudo','runuser','-l','upd','-c',
               'tar zxf /tmp/upd.tgz -C /home/upd/']
    run(cmdArgs)

    # RUNNING run1.sh as ecc from python does not work,
    # so running each individually - jtk
    #
    # ++++++++ FROM run1.sh ++++++++
    # tar -xzf tunnel.tgz -C ~/
    # echo "go to $HOME/data/tunnel/ to configure tunnel client - pick either amd64 or arm64 to run based on your device"
    # echo "to automatically restart tunnel client, you need to install monit or similar app and configure it properly"
    # if [ -f "/home/ecc/ros-docker/docker-compose.yml" ]
    # then
    #     sudo docker-compose -f /home/ecc/ros-docker/docker-compose.yml stop
    # fi
    # sudo groupadd -g 9990 appuser
    # sudo useradd -m -s /bin/bash upd
    # sudo usermod -L -a -G sudo,docker,appuser upd
    # sudo tar -xzf upd.tgz -C /home/upd/
    # echo "next use visudo to add line on bottom of sudoers file: upd ALL=(ALL) NOPASSWD: ALL"
    # sleep 5
    # sudo EDITOR=nano visudo
    # sudo -s -u upd
    #
    # OLD
    # tar -xzf tunnel.tgz -C ~/
    # sudo groupadd -g 9990 appuser
    # sudo useradd -m -s /bin/bash upd
    # sudo usermod -L -a -G sudo,docker,appuser upd
    # sudo tar -xzf upd.tgz -C /home/upd/
    # echo "next use visudo to add line on bottom of sudoers file: upd ALL=(ALL) NOPASSWD: ALL"
    # sleep 5
    # sudo EDITOR=nano visudo
    # sudo -s -u upd
    # +++++++++++++++++++++++++++++++

    # print("Finished run1 -------")


# ------------------------------------------------------------
# Install and configure tunnel client
# Sec 4.3 - Install Tunnel Client
# ------------------------------------------------------------
def tunnel_install():
    print("Running tunnel_install -------")

    cmdArgs = ['sudo','cp','/tmp/ros1.tgz','/home/ecc/']
    run(cmdArgs)
    cmdArgs = ['sudo','runuser','-l','ecc','-c',
               'cd /home/ecc/; tar -xzf ros1.tgz -C .']
    run(cmdArgs)

    run1()

    # TODO: FIGURE OUT WHERE TO GET THE CERTS!!! - jtk
    # I DO THIS IN TUNNELC INSTALL
    # cmdArgs = ['sudo','cp','/tmp/ecc50.*','/home/ecc/data/tunnel']
    # run(cmdArgs)

    # cmdArgs = ['sudo','chown','ecc:ecc','/home/ecc/data/tunnel/ecc50*']
    # run(cmdArgs)

    # print("Finished tunnel_install -------")


# ------------------------------------------------------------
# Set up twin user
# ------------------------------------------------------------
def configure_twin():
    # print("Running configure_twin -------")

    # FIRST we need to remove the duplicates in this file,
    # since this cannot be handled by anything else!
    cmdArgs = ['cp','/home/upd/ros-docker/updatec.json','/tmp/updatec.bak']
    run(cmdArgs)
    cmdArgs = ['cp','/home/upd/ros-docker/updatec.json','/tmp/updatec.tmp']
    run(cmdArgs)

    tmpfile = '/tmp/updatec.tmp'
    finfile = '/home/upd/ros-docker/updatec.json'

    prev_line = set()
    outfile = open(finfile, "w")
    for line in open(tmpfile, "r"):
        if line not in prev_line:
            outfile.write(line)
            prev_line.add(line)
    outfile.close()

    # I should do this with file open
    cmdArgs = ['sudo','sed','-i','s/"cloudIot": "false"/"cloudIot": "true"/g',
               '/home/upd/ros-docker/updatec.json']
    run(cmdArgs)

    with open("/tmp/ecc_mgr_agent.json", "r") as content:
        json_data = json.load(content)
        token = json_data['azure_iot_hub']['token']
        hubId = json_data['azure_iot_hub']['iotHub']

    # TODO: The deviceId OR the DP needs to be provided in ecc_mgr_agent.json.
    # If DP is provide, we need to build devId from DP+licensekey - jtk
    # HARDCODE FOR NOW JUST TO DEVELOP AND TEST! - jtk
    # devId = "ecc50"

    iotConf = '/home/upd/roshadata201805090000.999/iotconf/iotmqtt.json'
    with open(iotConf, 'r') as file:
        json_data = json.load(file)
        json_data['deviceId'] = deviceId
        json_data['sasToken'] = token
        json_data['iotHub'] = hubId

    with open(iotConf, 'w') as file:
        json.dump(json_data, file, indent=0)
        
    cmdArgs = ['sudo','docker','inspect','-f','{{.State.Running}}',
               'twin201805090000.999']
    result = run(cmdArgs, stdout=PIPE, stderr=PIPE, universal_newlines=True)
    if 'true' not in result.stdout:
        sys.exit("ERROR: twin container twin201805090000.999 is not running")

    sleep(30)

    print("Restarting twin1 container -------")
    #cmdArgs = ['sudo','docker-compose','-f',
    #           '/home/upd/ros-docker/files/docker-compose.yml',
    #           'restart','twin1']
    #run(cmdArgs,stdout=DEVNULL,stderr=DEVNULL)

    cmdArgs = ['sudo','docker','container',
               'restart','twin201805090000.999']
    run(cmdArgs,stdout=DEVNULL,stderr=DEVNULL)
    # print("Restarting updatec to 'disengage' from the update server? -------")
    # Restart updatec to 'disengage' from the update server?
    cmdArgs = ['sudo','pkill','updatec3']
    run(cmdArgs)
    cmdArgs = ['sudo','service','monit','reload']
    run(cmdArgs)
    # print("Finished configure_twin -------")


# ------------------------------------------------------------
# updatec3 log checker
# ------------------------------------------------------------
def updatec_log_check(logtext, maxcnt):
    # print("Running updatec_log_check -------")
    
    # VERY NON-PYTHONIC HACK
    # TO FIND
    # "If all containers are running"
    count = 0
    # last time it took more that 6 min
    cmdArgs = ['sudo','grep','-q',logtext,'/home/upd/ros-docker/tmpupd.log']
    print("------- Waiting up to {} minutes for '{}' message in /home/upd/ros-docker/tmpupd.log".format(maxcnt,logtext))
    while True:
        sleep(60)
        count += 1
        if run(cmdArgs,stdout=DEVNULL,stderr=DEVNULL).returncode != 0:
            if count == maxcnt:
                sys.exit("ERROR: Did not find '{}' in tmpupd.log in {} minutes".format(logtext,count))
        else:
            print("------- Found '{}' message in less than {} minutes".format(logtext,count))
            return

    # print("Finished updatec_log_check -------")


# ------------------------------------------------------------
# Install update client app
# Sec 4.5 Install Update Client App
# ------------------------------------------------------------
def updatec_install():
    print("Running updatec_install -------")
    currentUser = getpass.getuser()
    print("current user: " + getpass.getuser())

    cmdArgs = ["sudo",'runuser','-l','root','-c',"sudo cat /tmp/docker_password.txt | sudo docker login --username started489361id --password-stdin"]
    if run(cmdArgs,stdout=DEVNULL).returncode != 0:
        sys.exit("Docker login failed")

    cmdArgs = ['sudo','runuser','-l','upd','-c',
               'cd /home/upd/ros-docker; ./run2.sh']
    run(cmdArgs)

    cmdArgs = ['sudo','runuser','-l','upd','-c',
               'cp /tmp/{}.crt /home/upd/ros-docker/'.format(deviceId)]
    run(cmdArgs)

    cmdArgs = ['sudo','runuser','-l','upd','-c',
               'cp /tmp/{}.key /home/upd/ros-docker/'.format(deviceId)]
    run(cmdArgs)

    cmdArgs = ['sudo','chmod','600','/home/upd/ros-docker/{}.key'.format(deviceId)]
    run(cmdArgs)

    cmdArgs = ['sudo','chown','upd:upd','/home/upd/ros-docker/{}.key'.format(deviceId)]
    run(cmdArgs)

    cmdArgs = ['sudo','chown','upd:upd','/home/upd/ros-docker/{}.crt'.format(deviceId)]
    run(cmdArgs)

    cmdArgs = ['cp','/home/upd/ros-docker/updatec.json',
               '/home/upd/ros-docker/updatec.json.bak.2']
    run(cmdArgs)

    updatecConf = '/home/upd/ros-docker/updatec.json'
    eccCert = '{}.crt'.format(deviceId)
    eccKey = '{}.key'.format(deviceId)

    with open(updatecConf, 'r') as file:
        json_data = json.load(file)
        json_data['ecctlscrt'] = eccCert
        json_data['ecctlskey'] = eccKey

    with open(updatecConf, 'w') as file:
        json.dump(json_data, file, indent=0)

    cmdArgs = ['sudo','runuser','-l','upd','-c',
               'cd /home/upd/ros-docker; ./add-monit.sh']
    run(cmdArgs)

    #cmdArgs = ["sudo",'runuser','-l','root','-c',"ecc-mgr-agent start &"]
    cmdArgs = ["sudo",'runuser','-l',currentUser,'-c',"ecc-mgr-agent start &"]
    output = run(cmdArgs,stdout=DEVNULL)
    
    
    if output.returncode != 0:  
        sys.exit("ecc-mgr-agent start failed")

    # updatec_log_check('mqtt1 container started',20)

    # Check to see if the twin config file is available, if not, assume v16
    # with twin container has not been installed.  This may be due to long
    # updatec3 'wait' and it is stuck at v1.  Kick it by restarting.
    # cmdArgs = ['sudo','ls',
    #            '/home/upd/roshadata201805090000.999/iotconf/iotmqtt.json']
    # if run(cmdArgs,stderr=DEVNULL,stdout=DEVNULL).returncode != 0:
    #     print("Restarting updatec3 -------------")
    #     cmdArgs = ['sudo','pkill','updatec3']
    #     run(cmdArgs)
    #     sleep(120)

    # See if twin is running, if not, restart updatec
    # cmdArgs = ['sudo','docker','inspect','-f','{{.State.Running}}',
    #            'twin201805090000.999']
    # result = run(cmdArgs, stdout=PIPE, stderr=PIPE, universal_newlines=True)
    # if 'true' not in result.stdout:
    #     print("Restarting updatec3 -------------")
      #fd  cmdArgs = ['sudo','pkill','updatec3']
      #fd  run(cmdArgs)

      #fd  cmdArgs = ['sudo','cat','/dev/null','>','/home/upd/ros-docker/tmpupd.log']
      #fd  run(cmdArgs)
      #fd  cmdArgs = ['sudo','monit','reload']
      #fd  run(cmdArgs)
        # Removing the file so we can use updatec_log_check
        # updatec3 will recreate it
        # cmdArgs = ['sudo','cat','/dev/null','>','/home/upd/ros-docker/tmpupd.log']
        # run(cmdArgs)

    # updatec_log_check('twin1 container started',30)

    # configure_twin()

    #fd cmdArgs = ['sudo','runuser','-l','upd','-c',
    #fd            'cd /home/upd/ros-docker/; ./setMqttHost.sh']
    #fd run(cmdArgs)

    cmdArgs = ['sudo','runuser','-l','upd','-c',
               'cd /home/upd/ros-docker/; ./set-zwave-dev.sh']
    run(cmdArgs)
	
    # configure_twin()

    # cmdArgs = ['sudo','pkill','updatec3']
    # run(cmdArgs)

    cmdArgs = ['sudo','service','monit','reload']
    run(cmdArgs)
    
	# ++++++++ FROM run2.sh ++++++++
    # sudo chmod -R 700 /home/upd
    # mkdir -p /home/upd/roshadata201805090000.999/{config,face1,info}
    # mkdir -p /home/upd/roshadata201805090000.999/face1/{img,people}
    # sudo cp -r /roshadata201805090000.999 /home/upd/
    # sudo chown -R upd:appuser /home/upd/roshadata201805090000.999
    # sudo chmod -R 770 /home/upd/roshadata201805090000.999
    # sudo cp /home/ecc/ros-docker/docker-compose.yml /home/upd/ros-docker//home/upd/ros-docker/tmp.logfiles/
    # sudo cp -r /home/ecc/.local /home/upd/
    # sudo cp -r /home/ecc/.docker /home/upd/
    # sudo chown -R upd:upd /home/upd/.local
    # sudo chown -R upd:upd /home/upd/.docker
    # sudo chown -R upd:upd /home/upd/ros-docker
    # sudo chmod 700 /home/upd/ros-docker
    # sudo chmod 700 /home/upd/ros-docker/*.sh
    #
    # OUTPUT="$(uname -m)"
    # if [[ ${OUTPUT} =~ .*aarch64.* ]]
    # then
    #     mv updatec3.arm updatec3
    #     rm updatec3.amd
    # else
    #     mv updatec3.amd updatec3
    #     rm updatec3.arm
    # fi
    # chmod 700 updatec3
    # +++++++++++++++++++++++++++++++

    print("Finished updatec_install -------")


# ------------------------------------------------------------
# Tunnel client setup
# Sec 4.8 - Tunnel Client Setup
# ------------------------------------------------------------
def tunnelc_setup():
    print("Running tunnelc_setup -------")

    cmdArgs = ['sudo','runuser','-l','ecc','-c',
               'cp /tmp/{}.crt /home/ecc/data/tunnel/'.format(deviceId)]
    run(cmdArgs)

    cmdArgs = ['sudo','runuser','-l','ecc','-c',
               'cp /tmp/{}.key /home/ecc/data/tunnel/'.format(deviceId)]
    run(cmdArgs)

    cmdArgs = ['sudo','chown','ecc:ecc','/home/ecc/data/tunnel/{}.key'.format(deviceId)]
    run(cmdArgs)

    cmdArgs = ['sudo','chown','ecc:ecc','/home/ecc/data/tunnel/{}.crt'.format(deviceId)]
    run(cmdArgs)

    cmdArgs = ['cp','/home/ecc/data/tunnel/tunnel.yml.example',
               '/home/ecc/data/tunnel/tunnel.yml']
    run(cmdArgs)

    pip_install("pyyaml")
    import yaml

    # TODO: ecc50 IS TEMP FOR TESTING - jtk
    # THIS 'eccNum' is required to append to '399' as the port
    # we need to make a decision
    # HARDCODED TO '14' FOR DEVELOPMENT AND TEST ONLY - jtk
    # IF we expand to '3-digit', the will need to change slightly,
    # IF we come up with a REAL solution, this will change immensely. - jtk
    # eccNum = '50'
    # eccName = 'ecc' + eccNum

    tunDir = '/home/ecc/data/tunnel/'
    tunConf = tunDir + 'tunnel.yml'
    eccCert = tunDir + deviceId + '.crt'
    eccKey = tunDir + deviceId + '.key'
    eccHost = deviceId + '.tss.edgecloud.us'
    tunPort = '0.0.0.0:' + str(tunnelPort)

    with open(tunConf, "r") as file:
        yaml_data = yaml.load(file)
        yaml_data['tls_crt'] = eccCert
        yaml_data['tls_key'] = eccKey
        yaml_data['tunnels']['webui']['host'] = eccHost
        yaml_data['tunnels']['ssh']['remote_addr'] = tunPort

    with open(tunConf, "w") as file:
        yaml.dump(yaml_data, file)

    # ADDED by jtk
    cmdArgs = ['sudo','chown','ecc:ecc','/home/ecc/data/tunnel/tunnel.yml']
    run(cmdArgs)

    # this is done several times need to save a global
    arch = str((run(['uname','-m'],stdout=PIPE).stdout))

    if 'aarch64' in arch:
        cmdArgs = ['cp','/home/ecc/data/tunnel/tunnel.arm64',
                   '/home/ecc/data/tunnel/tunnelc']
    elif '86' in arch:
        cmdArgs = ['cp','/home/ecc/data/tunnel/tunnel.amd64',
                   '/home/ecc/data/tunnel/tunnelc']
    else:
        sys.exit("System arch unknown")

    run(cmdArgs)

    print("Finished tunnelc_setup -------")


# ------------------------------------------------------------
# Configure Monit for EMA
# ------------------------------------------------------------
def monit_ema():
    print("Running monit_ema -------")
    print("THIS IS NOT FINISHED, DON'T RUN UNTIL YOU FIX THE ftext FOR EMA!")

    # Monit should already me installed and running, but you _could_ go check

    # TODO: ADD THE EMA to monit - jtk
    # MODIFY TEXT BELOW FOR EMA!!!!!!!!!!

    fname = "/tmp/ema"
    ftext = '''
    check process ema
       matching "tunnelc"
       start program = "/bin/bash -c \'/home/ecc/data/tunnel/tunnelc -config /home/ecc/data/tunnel/tunnel.yml start-all > /home/ecc/data/tunnel/tc.log 2>&1\'"
       stop program = "/usr/bin/pkill tunnelc"
    '''

    monitfile = open(fname,"w")
    monitfile.write(ftext)
    monitfile.close()

    cmdArgs = ["sudo","chown","root:root",fname]
    run(cmdArgs)
    cmdArgs = ["sudo","chmod","600",fname]
    run(cmdArgs)
    cmdArgs = ["sudo","mv",fname,"/etc/monit/conf.d/"]
    run(cmdArgs)
    cmdArgs = ["sudo","monit","reload"]
    run(cmdArgs)

    print("Finished monit_ema -------")


# ------------------------------------------------------------
if __name__ == '__main__':

    # ------------------------------------------------------------
    # ------------------------------------------------------------
    # Sec 0.0 - PRE-INSTALL SETUP (not in docs)
    # ------------------------------------------------------------
    # ------------------------------------------------------------

    # ------------------------------------------------------------
    os_check()
    # ------------------------------------------------------------
    cmdArgs = ['sudo','add-apt-repository','universe']
    run(cmdArgs)
    # ------------------------------------------------------------
    # Install and run the EMA script with activate option
    # to provision user and get user creds
    # ------------------------------------------------------------
    email, pswd, lkey = get_creds()
    ema_install()
    ema_activate(email,pswd,lkey)

    # TODO: DP? is Suresh going to add that to the response?
    deviceId = lkey # like HPN-huj34ld4kfh or something

    # TODO: sometime later, at the end, we need to:
    #ema_run()
    # BETTER add it to monit and reload monit
    # ------------------------------------------------------------

    # ------------------------------------------------------------
    # END PRE-INSTALL SETUP
    # ------------------------------------------------------------

    # ------------------------------------------------------------
    # ------------------------------------------------------------
    # Sec 3 - Linux Setup
    # Install various tools and other configuration changes
    # ------------------------------------------------------------
    # ------------------------------------------------------------

    # ------------------------------------------------------------
    # Sec 3.1 - Bionic 18.04
    # ------------------------------------------------------------

    apt_install("net-tools")
    bash_comp()
    # host_name()
    apt_install("ssh")
    apt_install("monit")

    # ------------------------------------------------------------
    # Add and configure ECC user
    # ------------------------------------------------------------
    # SHOULD ENCRYPT THE PASSWORD FIRST!!!!!!!!!!
    adduser_pswd("ecc","prodea2435")
    cmdArgs = ['sudo','usermod','-a','-G','sudo','ecc']
    run(cmdArgs)

    # ------------------------------------------------------------
    # Manage root user - DON'T DO THIS
    # ------------------------------------------------------------
    ##
    ## su ecc
    ##
    ## - Disable root: ???
    ## sudo passwd -l root && sudo passwd -d root  ***** PROMPTS FOR PASSWORD
    ##
    ## - and confirm that action by typing ecc user's password
    ##

    # ------------------------------------------------------------
    # TODO: Here, we really need to prompt for timezone.  But,
    # it is highly unlikely the user will know what exact value to
    # use, since this cmd accepts the IANA 'tz' format, such as
    # 'America/Chicago'.
    # For now, hardcoding to 'America/Chicago'
    tz = 'America/Chicago'
    set_tzone(tz)
    # ------------------------------------------------------------

    # ------------------------------------------------------------
    # DON'T BOTHER
    # ------------------------------------------------------------
    # ssh_mods()
    # ------------------------------------------------------------

    # ------------------------------------------------------------
    # THIS HAS ISSUES WITH SOME INSTALLS, SO 
    # WE ARE JUST GOING TO MAKE IT A PRE-REQ - jtk
    # apt_upgrade()
    # doc says reboot now, but cannot, need to wait until end
    # ------------------------------------------------------------

    # ------------------------------------------------------------
    # ------------------------------------------------------------
    # Sec 4 - Docker and App Installation
    # ------------------------------------------------------------
    # ------------------------------------------------------------

    # ------------------------------------------------------------
    # Sec 4.2/4.6 - Install Docker Daemon (4.2 links to 4.6)
    # ------------------------------------------------------------
    docker_install()
    # ------------------------------------------------------------

    # ------------------------------------------------------------
    # Sec 4.3 - Install Tunnel Client
    # ------------------------------------------------------------
    tunnel_install()
    # ------------------------------------------------------------

    # ------------------------------------------------------------
    # Sec 4.4/4.7 - Install Docker Compose (4.4 links to 4.7)
    # ------------------------------------------------------------
    docker_comp_install()
    # ------------------------------------------------------------

    # ------------------------------------------------------------
    # Sec 4.5 Install Update Client App
    # ------------------------------------------------------------
    updatec_install()
    # ------------------------------------------------------------

    # ------------------------------------------------------------
    # Sec 4.8 - Tunnel Client Setup
    # ------------------------------------------------------------
    tunnelc_setup()

    # ------------------------------------------------------------
    # Sec 6.1 - Install and Configure Monit
    # already installed from /home/upd/ros-docker/add-monit.sh
    # just need to do for EMA
    # ------------------------------------------------------------
    # monit_ema()
    # ------------------------------------------------------------
