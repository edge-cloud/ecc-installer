#!/bin/bash
cd /tmp
sudo apt install -y apt-transport-https ca-certificates curl software-properties-common
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=arm64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt update
sudo apt install docker-ce curl wget python3-apt  libffi-dev python-openssl python3-pip python3 -y
sudo curl -L "http://ec2-54-218-139-231.us-west-2.compute.amazonaws.com:8080/docker-compose-Linux-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

wget https://gitlab.com/edge-cloud/ecc-installer/raw/master/ros1.tgz
wget https://gitlab.com/edge-cloud/ecc-installer/raw/master/ecc_installer.py

# add monit for ema 
sudo apt-get install monit -y
sudo wget -O /etc/monit/conf.d/ema https://gitlab.com/edge-cloud/ecc-installer/raw/master/monit_ema
sudo monit reload

chmod +x ecc_installer.py
sudo -H ./ecc_installer.py

exit 0
